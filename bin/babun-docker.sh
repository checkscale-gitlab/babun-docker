#! /bin/bash

__create_docker_vm() {
  docker-machine rm -f $VM &> /dev/null || :
  rm -rf ~/.docker/machine/machines/$VM

  # To Fix: "Unable to get the local Boot2Docker ISO version:  Did not find prefix "-v" in version string"
  # see: https://github.com/boot2docker/boot2docker/issues/1347
  # printf '\x2D\x76\x31\x38\x2E\x30\x39\x2E\x30\x20' | dd of=boot2docker.iso bs=1 seek=32819 count=10 conv=notrunc 

  # If there's no other machine with the same cidr (Classless Inter-Domain Routing), the machine should always get the .100 IP upon start.
  docker-machine create -d virtualbox \
    --virtualbox-hostonly-cidr "192.168.99.1/24" \
    --virtualbox-share-folder "\\\\?\\c:\\:/cygdrive/c" \
    --engine-env HTTP_PROXY=$HTTP_PROXY \
    --engine-env HTTPS_PROXY=$HTTPS_PROXY \
    --engine-env NO_PROXY=$no_proxy \
    $VM

  # make /c an alias for /cygdrive/c so that accessing the shared folder 
  # work seamlessly either from babun or from the docker terminal
  docker-machine ssh $VM "sudo ln -s /cygdrive/c /c"

  if [[ -n $STATIC_IP ]]; then
    __setup_static_ip $STATIC_IP $VM
  fi

  __setup_shared_folders
  __setup_docker_environment
}

### WIP ###
# https://stackoverflow.com/questions/29693993/how-to-set-a-specific-fixed-ip-address-when-i-create-a-docker-machine-or-conta/29701316
# https://stackoverflow.com/questions/34336218/is-there-a-way-to-force-docker-machine-to-create-vm-with-a-specific-ip/34336489
__setup_static_ip() {
  # disabled.
  # Despite the effort made by this method to set a static ip to de default virtual machine
  # the ip address of the virtual machine is still reset by the DHCP from time to time.
  return;

  echo "seting static IP to 192.168.99.${1:-100}"
  echo "ifconfig eth1 192.168.99.${1:-100} netmask 255.255.255.0 broadcast 192.168.99.255 up" \
    | docker-machine ssh ${2:-default} sudo tee /var/lib/boot2docker/bootsync.sh > /dev/null

  docker-machine restart $VM
  yes | docker-machine regenerate-certs $VM
}

__run_docker_vm() {
  docker-machine start $VM
  yes | docker-machine regenerate-certs $VM

  __setup_volume
  __setup_docker_environment
}

__setup_docker_environment() {
    # Transient Environment Variables, available in current terminal
    eval "$(docker-machine env --shell=zsh --no-proxy $VM)"
    # Persistent Environment Variables, available in all future terminals (set Windows's env variables)
    eval "$(docker-machine env --shell=zsh --no-proxy $VM | sed -e "s/export/SETX/g" | sed -e "s/=/ /g")" &> /dev/null

    export DOCKER_HOST_IP=$(docker-machine ip $VM)
    (SETX DOCKER_HOST_IP $(docker-machine ip $VM)) &> /dev/null

    cat << EOF

                    ##         .
              ## ## ##        ==
          ## ## ## ## ##    ===
      /"""""""""""""""""\___/ ===
 ~~~ {~~ ~~~~ ~~~ ~~~~ ~~~ ~ /  ===- ~~~
      \______ o           __/
        \    \         __/
          \____\_______/

EOF

  echo "${BLUE}docker${NC} is configured to use the ${GREEN}${VM}${NC} machine with IP ${GREEN}$(docker-machine ip $VM)${NC}"
  echo
  echo
}

__setup_shared_folders() {
  # disabled, waiting for the fix of docker-machine issue #4027 (https://github.com/docker/machine/issues/4027)
  return;

  # Set up shared folders in VirtualBox
  for drive in $(echo $ADDITIONAL_DRIVES | tr '\n' ' ') ; do
    windows_drive=$(cygpath -d /$drive)
    # windows_drive2=$(cygpath -d /cygdrive/$drive)

    if [[ -z $(VBoxManage showvminfo $VM | grep "Name: '$drive, Host path: '$windows_drive'") ]] ; then
      echo "$babun_docker_feedback Setting VirtualBox shared folder $drive => $windows_drive"
      VBoxManage sharedfolder add $VM --name $drive --hostpath $windows_drive --automount
      # echo "$babun_docker_feedback Setting VirtualBox shared folder for drive $drive => $windows_drive2"
      # VBoxManage sharedfolder add $VM --name $drive --hostpath $windows_drive2 --automount
    fi
  done
}

__setup_volume() {
  # disabled, waiting for the fix of docker-machine issue #4027 (https://github.com/docker/machine/issues/4027)
  return;

  # Set up volumes
  if [[ -z ${babun_docker_volumes:+x} ]] ; then
    for drive in $(echo $ADDITIONAL_DRIVES | tr '\n' ' ') ; do
      echo "$babun_docker_feedback Mounting drive: $drive"
      echo "$babun_docker_feedback   mkdir -p /cygdrive/$drive/"
      docker-machine ssh $VM "sudo mkdir -p /cygdrive/$drive/"
      echo "$babun_docker_feedback   mount -t vboxsf $drive /cygdrive/$drive/"
      docker-machine ssh $VM 'sudo mount -t vboxsf -o "defaults,uid=`id -u docker`,gid=`id -g docker`,iocharset=utf8,rw"' "$drive /cygdrive/$drive/"

      echo "$babun_docker_feedback   sudo ln -s /cygdrive/$drive /$drive"
      docker-machine ssh $VM "sudo ln -s /cygdrive/$drive /$drive"
      # docker-machine ssh $VM 'sudo mount -t vboxsf -o "defaults,uid=`id -u docker`,gid=`id -g docker`,iocharset=utf8,rw"' "$drive /$drive/"
    done
  fi;
}

__invoke_docker_exe() {
  local use_winpty=0
  local previous_IFS=$IFS
  IFS='' # trick to preserve white spaces in the read command below

  "$docker_exe" "$@" 2>&1 | while read -r line; do
    echo $line
    if  [[ $line == "cannot enable tty mode on non tty input" ]] \
     || [[ $line == "the input device is not a TTY.  If you are using mintty, try prefixing the command with 'winpty'" ]] \
     || [[ $line == "Error: Cannot perform an interactive login from a non TTY device" ]] ; then
      use_winpty=1
    fi;
  done

  IFS=$previous_IFS

  # it seems that babun bash is using same variable as zsh
  retVal=${pipestatus[1]}

  if [[ $use_winpty == 1 ]] ; then
    echo "$babun_docker_feedback Using winpty"
    winpty "$docker_exe" "$@"
    retVal=$?
  fi

  return $retVal
}

docker_exe=$(which docker);

function docker-machine-set-static-ip() {
  __setup_static_ip "$@"
  __setup_docker_environment
}

function docker {
  VBoxManage list vms | grep \"$VM\" &> /dev/null
  if [ $? -eq 1 ]; then
    __create_docker_vm
  fi

  if [ "$( set +e ; docker-machine status $VM )" != "Running" ]; then
    __run_docker_vm
  fi

  __invoke_docker_exe "$@"
}
