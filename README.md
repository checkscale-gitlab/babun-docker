# babun-docker

**babun-docker** allow using [Docker-Toolbox](https://www.docker.com/toolbox) from a [Babun](http://babun.github.io/) terminal in Windows.

**babun-docker** installs [winpty](https://github.com/rprichard/winpty), sets the environment variables and creates a function to embed `docker` to allow non-tty connections permitting running commands that "enter" in the container, as for example those that use `-it` and end in `bash`:

  ```bash
  docker run -it -v $(pwd):/var/www debian bash
  ```

**babun-docker** checks if the default docker-machine (the Virtual Machine) exists and is running, if not, it creates one, starts it and set the environment to use it.

You only have to make sure you run a normal `docker` command first to start and set up everything. For example:

  ```bash
  docker ps
  ```

**babun-docker** sets up a shared folder in VirtualBox for the `c:` drive and mounts it inside the virtual machine (docker-machine), to allow using volumes with Docker that permit using commands like:

  ```bash
  docker run -d -v $(pwd):/var/www ubuntu ping google.com
  ```

  **Note:** the mounting of additional drives (`d:`, `f:`, etc) inside the docker-machine virtual machine is disabled right-now and is waiting for the fix of docker-machine issue [#4027](https://github.com/docker/machine/issues/4027)


~~The shared folders (drives) inside the docker-machine (VirtualBox virtual machine) are mounted in two different directories to make it compatible with `docker` and `docker-compose`, so you can use normal relative volumes with `docker-compose`.~~ 

**Note**: After installing **babun-docker** (this program), you don't have to "use" another program. You can keep using the ```docker``` commands as normal. And after running a first `docker` command, you can use `docker-compose` as you would normally too.

## Installation

* Install [Docker-Toolbox](https://www.docker.com/products/docker-toolbox).
* Run the bundled Docker Quickstart Terminal that comes with Docker-Toolbox to make sure everything is working.
* Turn off the Docker-Toolbox Virtual Machine: run `docker-machine stop default` (or turn off the Virtual Machine `default` in VirtualBox) so that **babun-docker** can do all the needed automatic configurations with the VM turned off.
* Install [Babun](http://babun.github.io/) and start a terminal.
* Run the following command:

  ```
  curl -s https://gitlab.com/zartc/babun-docker/raw/master/setup.sh | source /dev/stdin
  ```
  The setup will:

  * Download and install Winpty to allow using Docker commands that enter a container
  * Create the `babun-docker-update` command to update **babun-docker**
  * Add itself to the ```~/.zshrc``` file to run at startup
  * Run (```source```) the script to fix Docker, wrapping it

* To be able to use **babun-docker** right away without having to close and open Babun run the update:

  ```
  babun-docker-update
  ```

* From Babun, use Docker as you would normally, for example:

  ```
  docker ps
  ```
  The `docker` wrapper will:

  * create/start the "default" docker machine
  * set the environment variables for that default docker-machine
  * use winpty if needed to connect to a tty session to avoid errors
  * Whenever it does something for you (automatically) you will see an output like:  
    `-- babun-docker: doing something`


## Updating

To update **babun-docker**, after following the installation instructions, run the command:

  ```
  babun-docker-update
  ```
----

## Docker Volumes with Babun

Here is an explanation of how volumes work in Docker and how they work in the Docker-Toolbox Virtual Machine: [Docker Volumes with Babun](https://github.com/tiangolo/babun-docker/wiki/Docker-Volumes-with-Babun).

That's what allows using commands like:
```
docker run -it -v $(pwd):/var/www debian bash
```

But all that is implemented automatically in **babun-docker**.

----

## Configurations

After installing **babun-docker**, you can configure things with variables in the `~/.babun-docker/custom-config.sh` file.

* ~~If you want to specify additional drives (other than `c:`) that should be mounted as VirtualBox shared folders inside your docker-machine virtual machine, then set the variable `ADDITIONAL_DRIVES` to a list of the drive names separated by spaces, as in: `ADDITIONAL_DRIVES="d f"`~~  

  **Note:** the mounting of additional drives (`d:`, `f:`, etc) inside the docker-machine virtual machine is disabled right-now and is waiting for the fix of docker-machine issue [#4027](https://github.com/docker/machine/issues/4027)
  - drive `c:` is always automaticly mounted
  - set the variable to "" (empty) if you don't want additional shared folders.

* If you want to change the virtual machine to use (if you have configured another virtual machine with `docker-machine`) you can set the variable `VM` to the name of your prefered virtual machine.

----

## What's new (zartc version)

#### 2018-01:
Disable support for the mounting of additional drives (`d:`, `f:`, etc) inside the docker-machine virtual machine, waiting for the fix of docker-machine issue [#4027](https://github.com/docker/machine/issues/4027).

----

## What's new (tiangolo version)

#### 2018-04-27:

Updates (`winpty`) and bug fixes. See PR [34](https://github.com/tiangolo/babun-docker/pull/34) by [murray02](https://github.com/murray02).

The return code is now preserved. See PR [31](https://github.com/tiangolo/babun-docker/pull/31) by [ptisserand](https://github.com/ptisserand).

#### 2016-11-09:
Now the shared folders are mounted in two directories inside the VirtualBox virtual machine to make it compatible with `docker-compose`.

You can start and set up **babun-docker** and all the shared folders with any `docker` command, as:

```bash
docker ps
```

And have a `docker-compose.yml` file with:

```yml
version: '2'
services:
  server:
    build: ./server
    volumes:
      - ./server/app:/app
    ports:
      - 8081:80
```

...note the relative mounted volume in `./server/app:/app`.

And then bring up your stack with:

```bash
docker-compose up -d
```

and it will work (because the shared folder paths that `docker-compose` uses are also mounted in the virtual machine).

#### 2016-08-17:
* Fix for the command `docker login`, see PR [24](https://github.com/tiangolo/babun-docker/pull/24) by [jpraet](https://github.com/jpraet).

#### 2016-07-05:
* Make `winpty` download file explicit, see PR [23](https://github.com/tiangolo/babun-docker/pull/23) by [murrayju](https://github.com/murrayju).
* Use the latest version of Winpty (0.4.0).

#### 2016-06-22:
* Fix for Docker Beta for Windows, see PR [#21](https://github.com/tiangolo/babun-docker/pull/21) by [@ronnypolley](https://github.com/ronnypolley).

#### 2016-04-21:
* Now you can run **babun-docker** in **Cygwin** (but I still recommend **Babun**).

#### 2016-04-20:
* Update winpty to latest version (and make old winpty installs to auto-update).
* Now you can use Bash instead of Zsh.

#### 2016-04-19:
* You can configure the VirtualBox installation path with the variable `babun_docker_virtualbox_bin`. Read more in the **Configurations** section below.

#### 2016-04-14:
* You can define which specific Windows drives to mount with the variable `babun_docker_volumes` (by default **babun-docker** tries to mounth them all). Read more in the **Configurations** section below.

* You can use a separate file in `~/.babun-docker/custom-config.sh` for custom configurations. Read more in the **Configurations** section below.

* Improved mounted volumes and ownership (with hints by [@olegweb](https://github.com/olegweb) ).

#### 2016-03-16:
* Support for Docker v1.10 (see PR [#9](https://github.com/tiangolo/babun-docker/pull/9) by [@mrkschan](https://github.com/mrkschan) ).

#### 2015-11-12:
* **babun-docker** automatically sets up shared folders in your VirtualBox virtual machine (docker-machine) for each of your Windows drives and mounts them inside the virtual machine, to allow using volumes (from any drive in your Windows, which is even more than what comes by default with the Docker-Toolbox) with commands like:

```
docker run -it -v $(pwd):/var/www debian bash
```

* You can configure if you want **babun-docker** to automatically set up VirtualBox shared folders and volumes with the environment variable ```babun_docker_setup_volumes```. Set it to "0" if you want to disable that. Read more in the **Configurations** section below.

* You can now specify the name of the docker-machine to use with the environment variable ```babun_docker_machine_name```, which is set by default to the "default" machine (named "default"). (No pun / tongue twister intended). Set that environment variable to the name of the machine that you want to use (e.g. ```babun_docker_machine_name='dev'```). Read more in the **Configurations** section below.

#### 2015-10-21:
* The installation of **babun-docker** clones the repository and sets up the environment to use the scripts inside it instead of writing it all to the ```~/.zshrc``` file.

* Running ```babun-docker-update``` will update that repository (will update **babun-docker**) and set up the environment again.




---

## License

This project is licensed under the terms of the MIT license.